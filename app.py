from flask import Flask, render_template, redirect, url_for, request, jsonify
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.wtf import Form
from wtforms.ext.sqlalchemy.orm import model_form
from flask.ext.basicauth import BasicAuth
from datetime import datetime
from flask_bootstrap import Bootstrap
from pygeoip import GeoIP

app = Flask(__name__)
app.config.from_pyfile("config.py")
Bootstrap(app)

basic_auth = BasicAuth(app)

from models import *
#db = SQLAlchemy(app)
db.init_app(app)
with app.app_context():
  db.create_all()
  db.session.commit()

gi = GeoIP('GeoIP.dat')

def log_ip():
  ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
  ua = request.headers.get('User-Agent')
  time = datetime.utcnow()
  url = request.url
  country = gi.country_name_by_addr(ip)
  u = TrafficLog(ip, ua, time, url, country)
  db.session.add(u)
  db.session.commit()
  return None

@app.route("/")
def hello():
  log_ip()
  return "Nothing to see here. Please move along."

@app.route("/all")
def all_data():
  log_ip()
  books = Book.query.all()
  films = Film.query.all()
  concerts = Concert.query.all()
  return render_template('all.html', books=books, films=films, concerts=concerts)

@app.route("/logs")
def show_logs():
  logs = TrafficLog.query.filter(TrafficLog.country != 'Belgium').order_by(TrafficLog.time.desc()).all()
  return render_template('logs.html', logs=logs)

BookForm = model_form(Book, base_class=Form)
FilmForm = model_form(Film, base_class=Form)
ConcertForm = model_form(Concert, base_class=Form)
ArticleForm = model_form(Article, base_class=Form)

def register(Entry, EntryForm):
  log_ip()
  html_file = Entry.__name__.lower() + "s.html"
  form = EntryForm()
  entry = Entry()
  if form.validate():
    form.populate_obj(entry)
    db.session.add(entry)
    db.session.commit()
    return redirect(url_for('all_data'))
  return render_template(html_file, form=form)

def insert_post(Entry, EntryForm):
  log_ip()
  html_file = "insert.html"
  form = EntryForm()
  entry = Entry()
  if form.validate():
    form.populate_obj(entry)
    db.session.add(entry)
    db.session.commit()
    return redirect(url_for('all_data'))
  return render_template(html_file, form=form)

@app.route("/books/", methods=['GET', 'POST'])
@basic_auth.required
def book_register():
  return register(Book, BookForm)

@app.route("/films/", methods=['GET', 'POST'])
@basic_auth.required
def film_register():
  return register(Film, FilmForm)

@app.route("/concerts/", methods=['GET', 'POST'])
@basic_auth.required
def concert_register():
  return register(Concert, ConcertForm)

@app.route("/insert/", methods=['GET', 'POST'])
@basic_auth.required
def insert_entry():
  return insert_post(Article, ArticleForm)

if __name__ == '__main__':
  app.run(host='127.0.0.1', port=8001)
