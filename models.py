from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Book(db.Model):
  __tablename__ = 'books'
  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(100), unique=True)
  author = db.Column(db.String(80), unique=False)
  year = db.Column(db.Integer, unique=False)
  country = db.Column(db.String(80), unique=False)

  def __repr__(self):
    return '{self.title} by {self.author}'.format(self=self)

class Film(db.Model):
  __tablename__ = 'films'
  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(100), unique=True)
  genre = db.Column(db.String(80), unique=False)
  country = db.Column(db.String(80), unique=False)
  year = db.Column(db.Integer, unique=False)

  def __repr__(self):
    return '{self.title} ({self.year})'.format(self=self)

class Concert(db.Model):
  __tablename__ = 'concerts'
  id = db.Column(db.Integer, primary_key=True)
  band = db.Column(db.String(100))
  date = db.Column(db.Date)
  location = db.Column(db.String(80))
  headliner = db.Column(db.Boolean)

  def __repr__(self):
    return '{self.band} @ {self.location}'.format(self=self)

class TrafficLog(db.Model):
  __tablename__ = 'logs'
  id = db.Column(db.Integer, primary_key=True)
  ip = db.Column(db.String(15))
  ua = db.Column(db.String(400))
  time = db.Column(db.DateTime, default=db.func.now())
  url = db.Column(db.String(40))
  country = db.Column(db.String(40))

  def __init__(self, ip, ua, time, url, country):
    self.ip = ip
    self.ua = ua
    self.time = time
    self.url = url
    self.country = country

class Article(db.Model):
  __tablename__ = 'articles'
  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(400))
  content = db.Column(db.Text())

  descr_types = db.Enum('Book', 'Film', 'Concert', name='Types')
  type = db.Column('type', descr_types)

class Tag(db.Model):
  __tablename__ = 'tag'
  id = db.Column(db.Integer, primary_key=True)
  tag = db.Column(db.String(40))

class ArticleTag(db.Model):
  __tablename__ = 'article_tag'
  article_id = db.Column(db.Integer, db.ForeignKey(Article.id), primary_key=True)
  tag_id = db.Column(db.Integer, db.ForeignKey(Tag.id), primary_key=True)

  article = db.relationship('Article', foreign_keys='ArticleTag.article_id')
  tag = db.relationship('Tag', foreign_keys='ArticleTag.tag_id')
